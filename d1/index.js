

//Statements, Syntax and Comments


//JS Statement usually end with semicolom(;). are not required but we will use it to help us train to locate where a statement ends.
//
console.log("hello world");


//Note: convention: Breaking up long statement into multiple lines is common practice.

/*

Multiple Comments

*/

//Variables
//It is used to contain data

//Declare Variables:
//Syntax: let/const variableName; var

//let/const/var is a keyword that is usually used in declaring a variable.

const myVariable = "Hello again";
console.log(myVariable);

console.log("Hello world")


let myVariable1

/*
Guides in writing variables:
1. we can use 'let/const/var' keyword followed by the varible name of your choice and use the assignment operator (=_ to assign a value.
2. Variable names should start with a lowercase character, use camelCase for multiple words
*/

//Best practices in naming variables:
//1. When naming variables. it is important to create variables that are descriptive and indicative of the data it contains

let firstName = "Jane" // good variable name
let lastName = "Doe"
let pokemon = 25000 // bad variable name

//2. It is better to start with lower case letter. Because there are several keywords in Js that start in capital letter.

//3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscore 
	//let first_name = "Mike";
	let product_description = "sample";

//Declaring and initializing variables
//Initializing variables - the instance when a variable is given its initial or starting value
//Syntax: let/const variableName = value;
let productName ="desktop computer";
console.log(productName)

let productPrice = 18999;
console.log(productPrice)


const interest = 3.539;

//Reassigning variable values
productName	= 'Laptop';
console.log(productName);

//let variable can be reassigned with another value;
//let variable cannot be re-declared within its scope;

let friend = "Kate";
let anotherFriend = "John"; //


//const cannot be updated or re-declared
//values of constants cannot be changed and will simply return an error
const pi = 3.14;
console.log(pi)

//Reassigning variables vs initializing variables
//let - we can declare a variable first

let supplier;
//Initialization is done after the variable has been declared

supplier = "John Smith Trading";
console.log(supplier);

//reassign its initial value
supplier = "Zuitt Store"
console.log(supplier);

//const variables are variables with constant data therefore we should not re-declare, re-assign or even declare a const variable without initialization.

//var vs. let/const

//var - is also used in declaring a variable. var is an ECMA script (ES1) feature ES1 (JS 1997)
//let/const was introduced as a new feature in ES6(2015)

//what makes let/const different from var?
//There are issues when we used var in declaring variables. One of this is hoisting.
//Hoisting is a JS default behavior of moving declarations to the top.

a = 5;
console.log(a);
var a;

//Scope essentially means where these variables are available to use

//let/const can be used as a local/global scope
//A block is a chunk of code bounded by {}. A block lives in curly bracse. Anything withing curly braces is block
//So a variables declared in a block with let/const is only available for use within that block

//global scope
var outerVariable = "hello";

{
	//block/local scope = {}
	var innerVariable = "helloagain";
	console.log(innerVariable);
}


//console.log(innerVariable); //is not defined


//Multiple Variable declarations

let productCode = 'DC017';
const productBrand ='Dell';
console.log(productCode, productBrand)

//const let = "Hello";


//DATA TYPES

//STRINGS
//Strings are a series of characters that create a word, a phrase, a sentence, or anything related to creating a text.
//Strings in JavaScript can be written using either a single (') or (") quotation.
let country = 'Philippines'
let province = "Metro Manila"
console.log(typeof country);
console.log(typeof province);

//Concatenating Strings
//Multiple string values can be combined to create a single string using the + symbol
let fullAddress = province + ', ' + country;
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

//Template literals (ES6) is the updated version of concatenation
//backticks `dvdv`
//expression ${}
console.log(`I live in the ${province}, ${country}`);

//Escape character (\) in strings in combination with other characters can produce different effects
// "\n" refers to creating a new line in between text

let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);


// \'

let message = 'John\'s employees went home early';
console.log(message);

message = "John's employees went home early"
console.log(message);

console.log(

	`Metro Manila

	"Hello"

	'Hello'

	Philippines ${province}`

	)



//NUMBERS
//Integers/Whole Numbers
let headCount = 23;
console.log(typeof headCount);

//Decimal Numbers/Fractions
let grade = 98.7;
console.log(typeof grade);

//Exponential Notation
let planetDistance = 2e10;
console.log(typeof planetDistance);


//Combining variable with number data type nd string
console.log("John's grade last quarter is " + grade);
console.log(`John's grade last quarter is ${grade}`)

//BOOLEAN
//Boolean values are normally used to store values relating to the state of certain things.
//This will be useful in further discussions about creating logic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log(typeof isMarried);
console.log(typeof inGoodConduct);

//OBJECTS

//Array
	//are a special kind of data that is used to store multiple values. It can sotre different data types but is normally used to store similar data types.

//similar data tyoes
//syntax: let/const arrayName = [elementA, elementB, etc]

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);
console.log(typeof grades);

//different data tyoes
//storing diff data types inside an array is not recommended because it will not make sense to join the context in programming.
let details = ["John", "Smith", 32, true];
console.log(details);

//indexing
console.log(grades[3], grades[0]);

//Reassigning array elements

//let anime = ['one piece', 'one punch man', 'attack on titan'];
//console.log(anime);
//anime[0] = 'kimetsu no yaiba'
//console.log(anime);

//using const

const anime = ['one piece', 'one punch man', 'attack on titan'];
console.log(anime);

anime[0] = 'kimetsu no yaiba'
console.log(anime);


//the keyword const is a little misleading. It does not define a constant value. It defines a constant reference to a value.
/*

Because of this you can not:

	Reassign a constant value, constant array, constant object

BUT YOU CAN:
	
	Change to elements of constant array
	Change the properties of constant object

*/

//OBJECTS
//syntax
/*
	let/const objectName = {propertyA: value, propertyB: value}

	gives an individual piece of information and it is called a property of the object

	They are used to create complex data that contains pieces of information that are relevant to each other.
*/

let objectGrades = {
	firstQuarter: 98.7,
	secondQuarter: 92.1,
	thirdQuarter: 90.2, 
	fourthQuarter: 94.6
};

let person = {
	fullName: 'Juan Dela Cruz',
	age: 35,
	isMarried: false,
	contact: ['091234557890', '8123 4567'],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}


}
console.log(person)

//dot notation
console.log(`${person.fullName} ${person.isMarried}`);
console.log(person.age);

console.log(person.contact[0])

console.log(person.address.city)


/*
Mini Activity


1. Create 2 variables named firstName and lastName.
                -Add your first name and last name as strings to its appropriate variables.
                -Log your firstName and lastName variables in the console at the same time.



2. Create a variable called sentence.
                -Combine the variables to form a single string which would legibly and -understandably create a sentence that would say that you are a student of Zuitt.
                -Log the sentence variable in the console.


3. Create a variable with a group of data.
                        -The group of data should contain names from your favorite food.

4. Create a variable which can contain multiple values of differing types and describes a single person.
                        -This data type should be able to contain multiple key value pairs:
                                firstName: <value>
                                lastName: <value>
                                isDeveloper: <value>
                                hasPortfolio: <value>
                                age: <value>
*/


//#1

let fName = "Anthea Elaine";
let lName = "Bonifacio";
console.log(`${fName} ${lName}`);
console.log(fName, lName);

//#2

let firstWords = "I am a student of";
let lastWords = "Zuitt.";
console.log(firstWords + ' ' + lastWords);
console.log(`I am a student of ${lastWords}`);

//or

let sentence = (`${fName} ${lName} a student in Zuitt`);
console.log(sentence);


//#3

let food = ['Nachos', 'Samosa', 'Cheesecake', 'Takoyaki'];
console.log(food);

//#4

let person1 = {
	firstName1: 'Will',
	lastName1: 'Smith',
	isDeveloper1: true,
	hasPortfolio1: true,
	age1: 29,
}
console.log(person1)



//NUMBER

let numb1 = 5
let numb2 = 6;
let numb3 = 5.5;
let numb4 = .5;

let numString1 = "5";
let numString2 = "6";

console.log(numString1 + numString2)//"56" strings were concatenated
console.log(numb1 + numb2) //11 


//TYPE COERCION/FORCED COERCION
//is the automatic or implicit conversion of values from one data type to another
console.log(numString1 + numb1) //"55" resulted in concatenation
//Adding/Concatenating a string and a number will result into a string


//Mathematical Operations (+, -, *, /, %(modulo-remainder))


//Modulo %
console.log(numb1 % numb2) // 5/6 = remainder 5
console.log(numb2 % numb1) // 6/5 = remainder 1

console.log(numb2 - numb1);

let product = 'The answer is ' + numb1 * numb2;
console.log(product)

//NULL
//It is used to intentionally express the absence of a value in a variable declaration/initialization
let girlfriend = null
console.log(girlfriend)

let myNumber = 0;
let myString = '';
console.log(myNumber)
console.log(myString)
//Using null comparead to a 0 value and an empty string is much better for readility purposes.


//UNDEFINED
//Represents the state of a variable that has been declared but without an assigned value.
let sampleVariable;
console.log(sampleVariable); //undefined

//One clear difference between undefined and null is that for undefined, a variable was created but was not provided a value.
//null means that a variable was created and was assigned a value that does not hold any value/amount


//FUNCTION
//in JS, it is a line/lines/block of codes that tell our app to perform a certain task when called/invoked.


//FUNCTION DECLARATION
	//defines a function with the function name and specified parameters

/*

Syntax:

	function functionName() {
		code block/statements. the block of code will be excuted once the function has been run/called/invoked.
	}
	
	function keyword ==> defined a js function
	function name ==> camelCase to name or function
	function block/statement ==> the statement which comprise the body of the funciton. This is where the code will be executed.
*/ 


	//function printName() {
	//	console.log("My name is John");
	//};

//FUNCTION EXPRESSION	
	//a function expression can be stored in a variable

	//Anonymouse function - a function without a name
	let variableFunction = function() {
		console.log("Hello Again")
	}


	let funcExpression = function func_name() {
		console.log("Hello!")
	}


//FUNCTION INVOCATION
	//The code inside a function is executed when the function is called/invoked.


//Let's invoke/call the function that we declared

//printName();


function declaredFunction(){
	console.log("Hello World")
}

declaredFunction();


//How about the function expression?
//They are always invoked/called using the variable name.

variableFunction();

//Function scoping

//JS scope
//global and local scope

//
//var faveCharacter = "Nezuko-chan";//global scope

function myFunction(){
	let nickName = "Jane"; //function scope
	console.log(nickName);

}

//console.log(faveCharacter);
myFunction();
//console.log(nickName);//not defined


//Variables defined inside a funciton are not accessible (visible) from outide the funciton
//var/let/const are quite similar when declared inside a function
//Variables declared globally (outside of the function) have a global scope or it can access anywhere.


function showSum() {
	console.log(6 + 6)
}

showSum();


//PARAMETERS AND ARGUMENTS
//"name" is called a parameter
//parameter => acts as a named variable/container that exists ONLY inside the function. Usually parameter is the placeholder of an actual value.
let name = "Jane Smith"

function printName(pikachu) {
	console.log(`My name is ${pikachu}`);
};


//argument is the actual value/date that we passed to our function
//We can also use a variable as an argument
printName(name);

//Multiple Parameters and Arguments

function displayFullName(fName, lName, age) {
	console.log(`${fName} ${lName} is ${age}`)
}


displayFullName("Judy", "Medalla", "jane", 67);
//Providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.
//Providing more arguments will not affect our function


//return keyword
	//The "return" statement allows the output of the function to be passed to the block of code that invoked the function
	//any line/block of code that comes after the return statement is ignored because it ends the function execution

	//return keyword is used so that a function may return a value
	//after returning the value, the next statements will stops its process

	function createFullName(firstName, middleName, lastName) {
		return `${firstName} ${middleName} ${lastName}`
		console.log("I will no longer run because the function's value/result has been returned")
	}

	//results of this function(return) can be saved into variable

	let fullName1 = createFullName("Tom", "Cruise", "Mapother");
	console.log(fullName1)

	//The result of a function without a return keyword will not save the result in a variable

	let fullName2 = displayFullName("William", "Bradley", "Pitt");
	console.log(fullName2)

	let fullName3 = createFullName("Jeffrey", "John", "Smith");
	console.log(fullName3);


/*
Mini Activity
	Create a function that can receive two numbers as arguments
		-display the quotient of the two numbers in the console
		-return the value of the division

	Create a variable called quotient.
		-This variable should be able to receive the result of division funciton

		Log the quotient variable's value in the console with the following message:

		"The result of the division is: <value of Quotient>"
			-use concatenation/template literals

		Take a screenshot of your console and send it in the hangouts.

*/ 

//Stretch goals
//get the number with the use of prompt
//prompt("What is your name")
//prompt("Enter first number")
//prompt("Enter second number")


function numbers(number1, number2) {
return `${number1}`/` ${number2}`;
}

let num1 = prompt(`First number:`);
let num2 = prompt(`Second number: `);
let quotient = numbers(8, 4);
console.log(`The result of the division is: ${quotient}`); 



//Function as an argument

function argumentFunction() {
	console.log("This function was as an argument before the message was printed")
}


function invokeFunction (pickachu) {
	argument();
}

invokeFunction(argumentFunction)











