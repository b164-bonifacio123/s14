//#2
console.log("Hello World");



//#3
let userDetails = {
	firstName: 'John',
	lastName: 'Smith',
	age: 30,
	hobbies: ["Biking", "Mountain Climbing", "Swimming"],
	workAddress: {
		houseNumber: 32,
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska",
	}
}

console.log(userDetails);

//#4 #6
function printUserInfo (firstName, lastName, age, hobbies, workAddress) {
	return `${firstName} ${lastName} ${age} ${hobbies} ${workAddress}`
}

//#5 #6
console.log(printUserInfo);


/*
function printUserInfo (firstName, lastName, age, hobbies, workAddress) {
	return `${firstName} ${lastName} ${age} ${hobbies} ${workAddress}`

let userDetails = {
	firstName: 'John',
	lastName: 'Smith',
	age: 30,
	hobbies: ["Biking", "Mountain Climbing", "Swimming"],
	workAddress: {
		houseNumber: 32,
		street: "Washington",
		city: "Lincoln",
		state: "Nebraska",
	}
}

console.log(userDetails);
*/












